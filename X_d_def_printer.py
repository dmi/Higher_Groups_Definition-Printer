#!/usr/bin/python

# For a description of this script, see function "display_help" below.


import sys
import itertools
import subprocess
import math


def display_help():
  print("+--------------------------------------------------------------+")
  print("|                                                              |")
  print("| This script has the purpose to write a LaTeX file            |")
  print("| which contains both the definition of the d-th level of a    |")
  print("| k-group written in the form X_d = { s[...] = (...) | ... }   |")
  print("| and its computation - and finally, to compile a PDF.         |")
  print("|                                                              |")
  print("| Invocation:                                                  |")
  print("| python X_d_def_printer.py k d                                |")
  print("|                                                              |")
  print("| Example:                                                     |")
  print("| python X_d_def_printer.py 3 4                                |")
  print("|                                                              |")
  print("| The output will be written into the files                    |")
  print("| 'X_k_d_definition.tex' and 'X_k_d_definition.pdf',           |")
  print("| where k and d are the respective command line arguments.     |")
  print("|                                                              |")
  print("| If you don't want to compile the tex-file to pdf,            |")
  print("| append the option '-do_not_compile' at the end.              |")
  print("|                                                              |")
  print("| Example:                                                     |")
  print("| python X_d_def_printer.py 3 4 -do_not_compile                |")
  print("|                                                              |")
  print("| Another function is to print a table of just the computed    |")
  print("| sets, which is achieved by adding the option '-table' at the |")
  print("| end. In this case, the specified k and d are the boundary-   |")
  print("| indices of the table.                                        |")
  print("|                                                              |")
  print("| Example:                                                     |")
  print("| python X_d_def_printer.py 4 6 -table                         |")
  print("|                                                              |")
  print("| More available options (only to be used with '-table') are:  |")
  print("| -landscape                                                   |")
  print("| -long_name                                                   |")
  print("|                                                              |")
  print("+--------------------------------------------------------------+")


def sign_op(r):
  # This function returns a string containing the leading sign of the real number (float) r.
  
  if (r >= 0):
    return "+"
  else:
    return "-"


def nCr(n, k):
  # This function returns the number of subsets of k elements in a set of n elements.
  
  factorial = math.factorial
  
  return (factorial(n) // factorial(k) // factorial (n - k))


def exponents(k, d):
  # This function returns the list of exponents of the factors of the product of groups which is equivalent to the d-th level of a k-group:
  # X^k_d = G^{exponents[1]} x PI_2^{exponents[2]} x ... x PI_k^{exponents[k]}.
  
  exponent_list = [0]
  
  for j in range(1, min(k, d) + 1):
    exponent_list.append(nCr(d, j))
  
  return exponent_list


def product_string(k, d):
  # This function returns a string containing the product of groups which is equivalent to the d-th level of a k-group:
  # X^k_d = G^{exponents[1]} x PI_2^{exponents[2]} x ... x PI_k^{exponents[k]}.
  
  if (d == 0):
    return "\\{pt\\}"
    
  else:
    
    exponent_list = exponents(k, d)
    
    s = "G^{%d}" % exponent_list[1]
    
    for j in range(2, min(k, d) + 1):
      s += " \\times \\Pi_{%d}^{%d}" % (j, exponent_list[j])
    
    return s


def short_name(k, d):
  # This function returns a string containing the short name of the d-th level of a k-group.
  
  return ("X_{%d}^{%d}" % (d, k))


def long_name(k, d):
  # This function returns a string containing the long name of the d-th level of a k-group.
  
  s = "\\left(\\mathcal{B}"
  
  if   (k == 2):
    s += "_{n_3}"
  elif (k == 3):
    s += "_{n_3, n_4}"
  elif (k == 4):
    s += "_{n_3, n_4, n_5}"
  elif (k >= 5):
    s += "_{n_3, \\dots , n_{%d}}" % (k + 1)
  
  s += "\\left(G, 1"
  
  for i in range(2, k + 1):
    s += "; \\Pi_{%d}, %d" % (i, i)
  
  s += "\\right)\\right)_{%d}" % d
  
  return s


if (len(sys.argv) < 3):
  display_help()
  sys.exit(1)

k = int(sys.argv[1])
d = int(sys.argv[2])

if (k < 1):
  display_help()
  print("\nERROR: Please choose k > 0.\n")
  sys.exit(1)


if ("-table" in sys.argv):
  
  output_file_name_base = "X_%d_%d_table" % (k, d)
  tex_file_name         = output_file_name_base + ".tex"
  
  file = open(tex_file_name, "w")
  
  file.write("\\documentclass{article}\n")
  file.write("\n")
  
  file.write("\\usepackage{a4wide}\n")
  file.write("\\usepackage{mathtools}\n")
  
  if ("-landscape" in sys.argv):
    file.write("\\usepackage{lscape}\n")
  
  file.write("\n")
  file.write("\\begin{document}\n")
  
  if ("-landscape" in sys.argv):
    file.write("\\begin{landscape}\n")
  
  if ("-long_name" in sys.argv):
    group_name = long_name
  else:
    group_name = short_name
  
  lines = []
  
  lines.append(" \\[\n")
  
  line = "  \\begin{array}{"
  
  for i in range(1, k + 1):
    line += "l"
    
  line += "}\n"
  
  lines.append(line)
  
  for i in range(d + 1):
    line = "   %s = %s" % (group_name(1, i), product_string(1, i))
    
    for j in range(2, k + 1):
      line += " & %s = %s" % (group_name(j, i), product_string(j, i))
    
    line += " \\\\\n"
    
    lines.append(line)
  
  lines[-1] = lines[-1][0:-4] + "\n"
  
  lines.append("  \\end{array}\n")
  lines.append(" \\]\n")
  
  for line in lines:
    file.write(line)
  
  if ("-landscape" in sys.argv):
    file.write("\\end{landscape}\n")
  
  file.write("\\end{document}\n")
  
  file.close()
  
else:
  
  output_file_name_base = "X_%d_%d_definition" % (k, d)
  tex_file_name         = output_file_name_base + ".tex"
  
  file = open(tex_file_name, "w")
  
  file.write("\\documentclass{article}\n")
  file.write("\n")
  file.write("\\usepackage{a4wide}\n")
  file.write("\\usepackage{mathtools}\n")
  file.write("\n")
  file.write("\\begin{document}\n")
  
  #file.write(" \\[\n")
  #file.write("  X_d = \\left\\{\n")
  #file.write("  \\begin{array}{l}\n")
  #file.write("   s[0 \\dots d] = \\left(x^1_{0 \\, 1}, \\dots, x^1_{d-1 \\: d}; \\: x^2_{0 1 2}, \\dots, x^2_{d-2 \\: d-1 \\: d}; \\: \\dots \\: ; \\: x^d_{0 \\dots d}")
  #file.write(    "\\right)\\! \\\\\n")
  #file.write("   \\textup{such that} \\\\\n")
  #file.write("   x^1_{..} \\in G \\textup{ and } x^j_{..} \\in \\Pi_j \\textup{ \\quad for all } j \\in \\{2, \\dots, d\\}, \\\\\n")
  #file.write("   \\big(dx^1\\big)\\left(x^2_{l_0 \\: l_1 \\: l_2}\\right) = q_2\\left(x^2_{l_0 \\: l_1 \\: l_2}\\right) \\textup{ \\quad for all } 2\\textup{-simplices }")
  #file.write(    " x^2_{l_0 \\: l_1 \\: l_2}, \\\\\n")
  #file.write("   \\big(\\alpha_{j - 1}\\left(x^1_{0 \\: 1}\\right)\\big)\\left(x^{j - 1}_{l_1 \\: \\dots \\: l_j}\\right) + \\sum_{m = 1}^{j} (-1)^m x^{j - 1}_{l_0 \\dots")
  #file.write(    " \\hat{l_m} \\dots l_j}\n")
  #file.write("    = q_j\\left(x^j_{l_0 \\: \\dots \\: l_j}\\right) + n_j\\left(x^1_{l_0 l_1}, \\dots, x^1_{l_{j - 1} l_j}\\right) \\\\\n")
  #file.write("   \\textup{\\quad \\quad for all } j\\textup{-simplices } x^j_{l_0 \\: \\dots \\: l_j} \\textup{ for all } j \\in \\{3, \\dots, d\\}\n")
  #file.write("  \\end{array}\n")
  #file.write("  \\right\\}\n")
  #file.write(" \\]\n")
  #file.write(" \n")
  file.write(" \\[\n")
  file.write("   S^k_d\\left(G, 1; \\Pi_2, 2; \\Pi_3, 3; \\dots\\right) \\mathrel{\\vcentcolon=} \\left\\{\n")
  file.write("   \\begin{array}{l}\n")
  file.write("     s[0 \\dots d] = \\left(x^1_{0 \\, 1}, \\dots, x^1_{d-1 \\: d}; \\: x^2_{0 1 2}, \\dots, x^2_{d-2 \\: d-1 \\: d}; \\: \\dots \\: ; \\: x^d_{0 \\dots d}")
  file.write(      "\\right)\\! \\\\\n")
  file.write("     \\textup{such that} \\\\\n")
  file.write("     x^1_{..} \\in G \\textup{ and } x^j_{..} \\in \\Pi_j \\textup{ for all } j \\in \\{2, \\dots, k\\} \\\\\n")
  file.write("     \\textup{\\quad and } x^j_{..} \\in \\{0\\} \\textup{ for all } j \\in \\{k + 1, \\dots, d\\}\n")
  file.write("   \\end{array}\n")
  file.write("   \\right\}\n")
  file.write(" \\]\n")
  file.write(" \n")
  file.write(" \\[\n")
  file.write("  X^k_d \\mathrel{\\vcentcolon=} \\left\\{ s \\in S^k_d \\left|\n")
  file.write("  \\begin{array}{l}\n")
  file.write("    \\big(dx^1\\big)(s_c) = q_2(s_c) \\textup{ \quad for all } 2 \\textup{-combinations } c\\\\\n")
  file.write("    \\textup{and} \\\\\n")
  file.write("    \\big(\\alpha_{j - 1}\\left(s_{(0, 1)}\\right)\\big)\\left(s_c^{\\hat{0}}\\right) + \\sum_{m = 1}^j (-1)^m s_c^{\\hat{m}}\n")
  file.write("     = q_j\\left(s_c\\right) + n_j\\left(\\nu_c(s)\\right) \\\\\n")
  file.write("    \\textup{\\quad for all } j\\textup{-combinations } c \\textup{ for all } j \\in \{3, \\dots, \\min(k + 1, d)\\}\n")
  file.write("  \\end{array} \\right. \\right\\}\n")
  file.write(" \\]\n")
  file.write(" \n")
  file.write(" \\[\n")
  
  if   (d == 0):
    file.write("   X_0^{%d} = \\{pt\\}\n" % k)
  elif (d == 1):
    file.write("   X_1^{%d} = G\n" % k)
    
  else:  
    
    file.write("  X_{%d}^{%d} = \\left\\{\n" % (d, k))
    file.write("  \\begin{array}{l}\n")
    
    lines = []
    
    if (d == 2):
      
      lines.append("   s[0 1 2] = \\left(x^1_{01}, x^1_{02}, x^1_{12}; \\: x^2_{012}\\right) \\\\\n")
      lines.append("   \\textup{such that} \\\\\n")
      lines.append("   x^1_{01}, x^1_{02}, x^1_{12} \\in G \\textup{ and } x^2_{012} \\in \\Pi_2, \\\\\n")
      
      line = "   x^1_{01} \\cdot x^1_{12} \\cdot \\left(x^1_{02}\\right)^{-1} = "
      
      if k >= 2:
        rhs = "q_2\left(x^2_{012}\\right)"
      else:
        rhs = "1"
      
      line += rhs + ", \\\\\n"
      
      lines.append(line)
      
    elif (d >= 3):
      
      if (d == 3):
        
        lines.append("   s = s[0123] = \\left(x^1_{01}, \\dots, x^1_{23}; \\: x^2_{0 1 2}, \\dots, x^2_{123}; \\: x^{3}_{0123}\\right) \\in S^{%d}_3\\! \\\\\n" % k)
        lines.append("   \\textup{such that} \\\\\n")
        
      else:
        
        line  = "   s = s[0 \\dots %d] = \\left(x^1_{01}, \\dots, x^1_{%d%d};" % (d, d - 1, d)
        line += "\\: x^2_{0 1 2}, \\dots, x^2_{%d%d%d}; \\: \\dots \\: ; \\: x^{%d}_{0 \\dots %d}\\right) \\in S^{%d}_{%d} \\\\\n" % (d - 2, d - 1, d, d, d, k, d)
        lines.append(line)
        
        lines.append("   \\textup{such that} \\\\\n")
      
      combinations = list(itertools.combinations(range(d + 1), 3))
      
      for c in combinations:
        
        line = "   x^1_{%d%d} \\cdot x^1_{%d%d} \\cdot \\left(x^1_{%d%d}\\right)^{-1} = " % (c[0], c[1], c[1], c[2], c[0], c[2])
        
        if k >= 2:
          rhs = "q_2\left(x^2_{%d%d%d}\\right)" % (c[0], c[1], c[2])
        else:
          rhs = "1"
        
        line += rhs + ", \\\\\n"
        
        lines.append(line)
      
      if (d > 2):
        for j in range(3, d + 1):
          if (j - 1 <= k):
            
            combinations = list(itertools.combinations(range(d + 1), j + 1))
            
            for c in combinations:
              
              line  = "   \\big(\\alpha_{%d}\\left(x^1_{0 \\: 1}\\right)\\big)\\left(x^{%d}_{" % (j - 1, j - 1)
              
              for m in range(1, j + 1):
                line += "%d" % c[m]
              
              line += "}\\right)"
              
              for m in range(1, j + 1):
                
                line += " %s x^{%d}_{" % (sign_op((-1)**m), j - 1)
                
                for l in range(j + 1):
                  if (l != m):
                    line += "%d" % c[l]
                
                line += "}"
              
              line += " = "
              
              if (j <= k):
                
                line += "q_{%d}\\left(x^{%d}_{" % (j, j)
                
                for l in c:
                  line += "%d" % l
                
                line += "}\\right) + "
              
              line += "n_{%d}\\left(\\nu_{(" % j
              
              for l in range(0, len(c) - 1):
                line += "%d, " % c[l]
              
              line += "%d" % c[-1]
              
              line += ")}(s)\\right)"
              
              line += ", \\\\\n"
              
              lines.append(line)
    
    lines[-1] = lines[-1][0:-5] + "\n"
    
    for line in lines:
      file.write(line)
    
    file.write("  \\end{array}\n")
    file.write("  \\right\\}\n")
  
  file.write(" \\]\n")
  file.write(" \n")
  file.write(" \\[\n")
  file.write("  X_{%d}^{%d} = %s = %s\n" % (d, k, long_name(k, d), product_string(k, d)))
  file.write(" \\]\n")
  
  file.write("\\end{document}\n")
  
  file.close()

if (not "-do_not_compile" in sys.argv):
  subprocess.call("(pdflatex " + tex_file_name + ")", shell = True)
