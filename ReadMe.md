Purpose
-------

This script has the purpose to write a LaTeX file which contains both the definition of the d-th level of a k-group written in the form

X_d = { s[...] = (...) | ... }

and its computation - and finally, to compile a PDF. I used it frequently while working on my master's thesis about higher groups.  
Here's an example screenshot of a resulting PDF:

<img src="X_3_4.png">



Usage
-----

Invocation:

`python X_d_def_printer.py k d`

Example:

`python X_d_def_printer.py 3 4`

The output will be written into the files 'X_k_d_definition.tex' and 'X_k_d_definition.pdf',
where k and d are the respective command line arguments.

If you don't want to compile the tex-file to pdf, append the option `-do_not_compile` at the end.

Example:

`python X_d_def_printer.py 3 4 -do_not_compile`

Another function is to print a table of just the computed sets, which is achieved by adding the option `-table` at the end.  
In this case, the specified k and d are the boundary-indices of the table.

Example:

`python X_d_def_printer.py 4 6 -table`

More available options (only to be used with `-table`) are:  
`-landscape`  
`-long_name`

Here's an example screenshot of the resulting table of `python X_d_def_printer.py 5 30 -table`:

<img src="X_5_30_table_rotated.png">

The script makes comparing different levels and groups easy:

<img src="X_3_4_vs_X_3_5.png">
